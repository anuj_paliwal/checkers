require 'debugger'
require 'colorize'
require 'unicode'

class InvalidMoveError < RuntimeError
end

class CheckerBoard
	attr_accessor :board

	def initialize
		@board = Array.new(8) { Array.new(8, nil) }
  end

  def play
    populate_board
  end

	def [](pos)
    x, y = pos[0], pos[1]
    self.board[x][y]
  end

  def []=(pos, piece)
    x, y = pos[0], pos[1]
    self.board[x][y] = piece
  end

	def populate_board
		populate_red
		populate_black
	end

	def populate_red
		(0..2).each do |row_idx|
			(0..7).each do |col_idx|
				if row_idx.even? && col_idx.even?
					self.board[row_idx][col_idx] = Piece.new('R', [row_idx, col_idx], self, false)
				elsif row_idx.odd? && col_idx.odd?
					self.board[row_idx][col_idx] = Piece.new('R', [row_idx, col_idx], self, false)
				end
			end
		end

	end

	def populate_black
		(5..7).each do |row_idx|
			(0..7).each do |col_idx|
				if row_idx.even? && col_idx.even?
					self.board[row_idx][col_idx] = Piece.new('B', [row_idx, col_idx], self, false)
				elsif row_idx.odd? && col_idx.odd?
					self.board[row_idx][col_idx] = Piece.new('B', [row_idx, col_idx], self, false)
				end
			end
		end
	end

	def render_board
		piece = { "B" => "\u26ab".encode('utf-8'), "R" => "\u25EF".encode('utf-8')}

    puts "\n"
    self.board.each_with_index do |row, row_idx|
			row.each_with_index do |el, col_idx|
				print " #{piece[el.color]} " if el.is_a?(Piece)
				print " * " if el.nil?
			end
			print "\n"
		end
  end

end

class Piece
	BLACK_DIRS = [[-1, 1], [-1, -1]]
	RED_DIRS = [[1, 1], [1, -1]]
  KING_DIRS = BLACK_DIRS + RED_DIRS

  attr_reader :color
	attr_accessor :pos, :board, :is_king

	def initialize(color, pos, board, is_king = false)
		@color = color
		@pos = pos
		@board = board
    @is_king = is_king
	end

  def pos=(coord)
    if self.color == "B" and coord[0] == 0
      self.is_king = true
    elsif self.color == "R" and coord[0] == 7
      self.is_king = true
    end

    @pos = pos
  end

  def dup
    test_board = CheckerBoard.new
    pieces = self.board.board.flatten.compact
    pieces.each do |piece|
      color, pos, board, is_king = piece.color, piece.pos, piece.board, is_king
      duped_piece = piece.class.new(color, pos, test_board, is_king)
      x, y = pos
      test_board.board[x][y] = duped_piece
    end

    test_board
  end

	def within_range?(position)
		position.all? { |coord| coord.between?(0, 7) }
	end

	def slide_moves(start_pos)
		x, y = start_pos
		self.color == "B" ? dirs = BLACK_DIRS : dirs = RED_DIRS
    dirs = KING_DIRS if self.is_king
		moves = []
		dirs.each do |dir|
			adj_diagonal = [x + dir[0], y + dir[1]]
			moves << adj_diagonal if within_range?(adj_diagonal)
		end

		moves.select { |coord| !self.board[coord] }
	end

	def jump_moves(start_pos)
		x, y = start_pos
		self.color == "B" ? dirs = BLACK_DIRS : dirs = RED_DIRS
    dirs = KING_DIRS if self.is_king
		moves = []
		dirs.each do |dir|
			adj_diagonal = [x + dir[0], y + dir[1]]
			two_diags_away = [x + 2 * dir[0], y + 2 * dir[1]]
      next unless self.board[adj_diagonal] && (self.board[adj_diagonal].color != self.color)

			moves << two_diags_away if within_range?(two_diags_away)
		end

		moves.select { |coord| !self.board[coord] }
	end

	def perform_slide(start_pos, end_pos)
		raise InvalidMoveError unless slide_moves(start_pos).include?(end_pos)
    self.board[start_pos] = nil
    self.board[end_pos] = self
    self.pos = end_pos
  end

	def perform_jump(start_pos, end_pos)
    raise InvalidMoveError unless self.jump_moves(start_pos).include?(end_pos)
    start_x, start_y = start_pos
    end_x, end_y = end_pos
    jumped_coord = []
    end_x > start_x ? jumped_coord[0] = start_x + 1 : jumped_coord[0] = start_x - 1
    end_y > start_y ? jumped_coord[1] = start_y + 1 : jumped_coord[1] = start_y - 1
    self.board[start_pos] = nil
    self.board[jumped_coord] = nil
    self.board[end_pos] = self
    self.pos = end_pos
	end

  def perform_moves(sequence)
    if valid_move_sequence?(sequence)
      perform_moves!(sequence)
    else
      raise InvalidMoveError
    end
  end

  def perform_moves!(sequence)
    valid_sequence = true
    distance = 0
    (0..sequence.length - 2).each do |idx|
      start_pos = sequence[idx]
      end_pos = sequence[idx + 1]
      distance = (start_pos[0] - end_pos[0]).abs

      if distance == 1
        perform_slide(start_pos, end_pos)
      elsif distance == 2
        perform_jump(start_pos, end_pos)
      else
        raise InvalidMoveError
      end
    end
  end

  def valid_move_sequence?(sequence)
    duped_board = dup
    start_x, start_y = sequence[0]
    begin
      duped_board[[start_x, start_y]].perform_moves!(sequence)
    rescue InvalidMoveError => e
          puts "Invalid move or move sequence"
          puts "Error was: #{e.message}"
          return false
    end

    true
  end

end

class Game
  attr_reader :board

  def initialize
    @board = CheckerBoard.new
    @board.populate_board
  end

  def play
  end

  def game_over?
    pieces = self.board.board.flatten.compact
    game_over = true
    red_pieces, black_pieces = 0, 0

    pieces.each do |piece|
      red_pieces += 1 if piece.color == "R"
      black_pieces += 1 if piece.color == "B"
      if (red_pieces > 0 || black_pieces > 0)
        game_over = false
        break
      end
    end

    game_over
  end

end

g = CheckerBoard.new

g.populate_board
g.render_board
# piece = Piece.new("R", [6, 1], g)
# g.board[6][1] = piece
# g.render_board
# p "is king: #{piece.is_king}"
# piece.perform_moves([[6, 1], [7, 0]])
#
# #p "is king: #{piece.is_king}"
# #g.board[1][0].perform_moves([[1, 0], [0, 1]])
#
# g.render_board
# p "is king: #{piece.is_king}"












#g.populate_board
#g.board[5][1].perform_slide([5, 1], [4, 2])
#g.board[5][7].perform_slide([5, 7], [4, 6])

# g.board[0][2] = Piece.new('B', [0, 2], g, true)
# g.board[3][1] = Piece.new('R', [3, 1], g, true)
# g.board[2][4] = nil
# g.board[3][3] = Piece.new('R', [3, 3], g, false)
# g.board[2][0] = nil
# g.render_board
# g.board[0][2].perform_moves([[0, 2], [2, 0], [4, 2], [2, 4]])
# g.render_board

#g.board[5][1].perform_moves([[5, 1], [4, 2], [3, 3]])

#g.render_board















#g = CheckerBoard.new

#p g.board[5][5].class
#g.board[4][4] = Piece.new('R', [4, 4], g)
#g.board[1][5] = nil
#g.render_board
#g.board[4][6] = Piece.new('B', [4, 6], g)
#g.board[3][7] = Piece.new('R', [3, 7], g)
#p g.board[5][5].slide_moves([5, 5])
#p g.board[4][6].jump_moves([4, 6])
#p g.board[5][5].perform_slide([5, 5], [3, 3])
#g.board[5][5].perform_moves!([[5, 5], [7, 7], [1, 5]])

#g.render_board


#g.board[5][1].perform_slide([5, 1], [4, 0])
#g.board[5][5].perform_jump([5, 5], [3, 3])

